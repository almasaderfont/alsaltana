jQuery(document).ready(function ($) {
  lazyLoad();
  $(".selectpicker").selectpicker();
  toggleSearch();
  datePickerInit();
  customDropdown();
  allSiteSwiperInit();
  // allSiteMixitupInit()
  verificationCodeSeprate();
  showPassword($);
  toggleSideMenuInSmallScreens($);
  stickyHeader($);
  counter();
  mixItUpInit();
  verificationCountdown();
});

// functions init

function datePickerInit() {
  $(".datepicker__").datepicker({
    inline: true,
  });
}

function counter() {
  $(".count_btn__").on("click", function (e) {
    e.preventDefault();
    const input = $(this).siblings("input");
    const minValue = parseInt(input.attr("min"));
    if ($(this).hasClass("decrement")) {
      if (input.val() > minValue) {
        input.val(parseInt(input.val()) - 1);
      }
    } else if ($(this).hasClass("increment")) {
      input.val(parseInt(input.val()) + 1);
    }
  });
}

function lazyLoad() {
  const images = document.querySelectorAll(".lazy-omd");

  const optionsLazyLoad = {
    //  rootMargin: '-50px',
    // threshold: 1
  };

  const preloadImage = function (img) {
    img.src = img.getAttribute("data-src");
    img.onload = function () {
      img.parentElement.classList.remove("loading-omd");
      img.parentElement.classList.add("loaded-omd");
      img.parentElement.parentElement.classList.add("lazy-head-om");
    };
  };

  const imageObserver = new IntersectionObserver(function (enteries) {
    enteries.forEach(function (entery) {
      if (!entery.isIntersecting) {
        return;
      } else {
        preloadImage(entery.target);
        imageObserver.unobserve(entery.target);
      }
    });
  }, optionsLazyLoad);

  images.forEach(function (image) {
    imageObserver.observe(image);
  });
}

function swiperInit(options) {
  const swiper = new Swiper(options.className + " .swiper-container", {
    spaceBetween: 30,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
    },
    rtl: $("html").attr("dir") === "rtl" ? true : false,
    pagination: {
      el: options.className + " .swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: options.className + " .swiper-button-next",
      prevEl: options.className + " .swiper-button-prev",
    },

    breakpoints: options.breakpoints,
    observer: options.observer,
    observeParents: options.observeParents,
    grid: options.grid,
    ...options,
  });

  lazyLoad();

  return swiper;
}

// function mixitupInit(id,options) {
//   mixitup(document.getElementById(id), {
//     selectors: {
//       control: ".filter_control__",
//       target: '.filter_item__',
//     },
//     ...options
// })

//   $(".filter_control__")[0].click()
// }

// function allSiteMixitupInit() {
//   const myOrdersPageOptions = {
//       selectors: {
//         control: ".filter_control__",
//         target: ".filter_item__",
//   }
// }

// mixitupInit('myOrdersPage',myOrdersPageOptions);
// }

function allSiteSwiperInit() {
  const homeOpinionSwiperBreakNormalPoints = {
    0: {
      slidesPerView: 1,
    },
    480: {
      slidesPerView: 2,
    },

    992: {
      slidesPerView: 3,
    },
  };

  const home_our_customers_opinions = {
    autoplay: true,
    className: ".home_our_customers_opinions",
    breakpoints: homeOpinionSwiperBreakNormalPoints,
    observer: true,
    observeParents: true,
  };

  const productSinglePagecustomers_opinions = {
    autoplay: true,
    className: ".offer-address",
    observer: true,
    observeParents: true,
  };

  swiperInit(home_our_customers_opinions);
  swiperInit(productSinglePagecustomers_opinions);
}

function verificationCodeSeprate() {
  const inputElements = [...document.querySelectorAll("input.code-input")];

  inputElements.forEach((ele, index) => {
    ele.addEventListener("keydown", (e) => {
      // if the keycode is backspace & the current field is empty
      // focus the input before the current. The event then happens
      // which will clear the input before the current
      if (e.keyCode === 8 && e.target.value === "") {
        inputElements[Math.max(0, index - 1)].focus();
      }
    });
    ele.addEventListener("input", (e) => {
      if (e.target.value === "") {
        inputElements[index].classList = "code-input";
      } else {
        inputElements[index].classList = "code-input active";
      }

      // take the first character of the input
      // this actually breaks if you input an emoji like 👨‍👩‍👧‍👦....
      // but I'm willing to overlook insane security code practices.
      const [first, ...rest] = e.target.value;
      e.target.value = first ?? ""; // the `??` '' is for the backspace usecase
      const lastInputBox = index === inputElements.length - 1;
      const insertedContent = first !== undefined;
      if (insertedContent && !lastInputBox) {
        // continue to input the rest of the string
        inputElements[index + 1].focus();
        inputElements[index + 1].value = rest.join("");
        inputElements[index + 1].dispatchEvent(new Event("input"));
      }
    });
  });
}

function showPassword($) {
  $(".show-password-button-om").on("click", function (e) {
    e.preventDefault();
    if ($(this).parent().find("input").attr("type") == "text") {
      $(this).parent().find("input").attr("type", "password");
      $(this).removeClass("show-om");
    } else {
      $(this).parent().find("input").attr("type", "text");
      $(this).addClass("show-om");
    }
  });
}

function toggleSideMenuInSmallScreens($) {
  // nav men activation
  $("#menu-butt-activ-om").on("click", function (e) {
    e.preventDefault();

    $("#navbar-menu-om").addClass("active-menu");
    $(".overlay").addClass("active");
    $("body").addClass("overflow-body");
  });

  // nav men close
  $(".close-button__ , .overlay ").on("click", function (e) {
    e.preventDefault();
    $("#navbar-menu-om").removeClass("active-menu");
    $(".overlay").removeClass("active");
    $("body").removeClass("overflow-body");
  });
}

function stickyHeader($) {
  let headerHeight = $("header").outerHeight();

  $("header").innerHeight(headerHeight);

  let lastScroll = 0;
  $(document).on("scroll", function () {
    let currentScroll = $(this).scrollTop();
    // side links
    if (currentScroll > headerHeight + 500 || screen.width < 500) {
      $(".side_links_section").addClass("active");
    } else {
      $(".side_links_section").removeClass("active");
    }

    // scroll down
    if (currentScroll < lastScroll && currentScroll > headerHeight + 500) {
      // add class avtive menu
      $(".fixed_header__").addClass("active_menu__");
      $(".fixed_header__").removeClass("not_active_menu__");
    } else if (
      currentScroll > lastScroll &&
      currentScroll > headerHeight + 500
    ) {
      // scroll up
      if ($(".fixed_header__").hasClass("active_menu__")) {
        $(".fixed_header__").removeClass("active_menu__");
        $(".fixed_header__").addClass("not_active_menu__");
      }
    } else {
      $(".fixed_header__").removeClass("active_menu__");
      $(".fixed_header__").removeClass("not_active_menu__");
    }
    lastScroll = currentScroll;
  });

  $(".arrow_button__").click(() => {
    $(".side_links_section").removeClass("active");
  });
}

function customDropdown() {
  $(".dropdown_button__").on("click", function (event) {
    const perantElement = $(this).closest(".custom_dropdown__");
    const menu = perantElement.find(".dropdown_menu__");
    let timeoutId;

    event.preventDefault();
    perantElement.toggleClass("show");

    menu.on("mouseleave", function () {
      timeoutId = setTimeout(function () {
        perantElement.removeClass("show");
      }, 750);
    });

    menu.on("mouseenter", () => clearTimeout(timeoutId));
  });
}

function mixItUpInit() {
  if (document.getElementById("myOrdersPage")) {
    mixitup($("#myOrdersPage"), {
      selectors: {
        control: ".filter_control__",
        target: ".filter_item__",
      },
    });
  }

  if (document.getElementById("proudctsType")) {
    mixitup($("#proudctsType"), {
      selectors: {
        control: ".filter_control__",
        target: ".filter_item__",
      },
    });
  }

  $(".filter").each((index, element) => {
    if (index === 0) element.click();
  });
}

function verificationCountdown() {
  const countAttrData = $(".count_down_wrapper__ .text__").attr(
    "count-data-with-second"
  );
  const percentage = 140 / countAttrData; // 140 is the value of the svg circle css to be complete circle
  let counter;
  setCounterReady();
  let interval = setInterval(counterInterval, 1000);

  function setCounterReady() {
    counter = countAttrData;
    toggleShowHideSendCodeAgainButtonAndCounterWrapper();
    setCounterNumberAndShape();
  }

  function counterInterval() {
    counter--;
    setCounterNumberAndShape();

    if (counter < 0) {
      clearInterval(interval);
      toggleShowHideSendCodeAgainButtonAndCounterWrapper(false);
    }
  }

  // Counter Wrapper And Send Code Again Button Can't Show Together
  function toggleShowHideSendCodeAgainButtonAndCounterWrapper(
    isShowCounterWrapper = true
  ) {
    if (isShowCounterWrapper) {
      $("#sendCodeAgain").hide();
      $("#countDownWrapper").show();
    } else {
      $("#sendCodeAgain").show();
      $("#countDownWrapper").hide();
    }
  }

  function setCounterNumberAndShape() {
    $(".count_down_wrapper__ .text__").html(
      (counter <= 9 ? "0" + counter : counter) + "S"
    );
    $("#counterCircleShape").css(
      "stroke-dasharray",
      `${counter * percentage} 350`
    );
  }

  $("#sendCodeAgain").click(() => {
    setCounterReady();
    verificationCountdown();
  });
}

function toggleSearch() {
  $("#searchLink , #searchIcon , #searchButton").on("click",function(e){
    e.preventDefault();
    $(".search_section").addClass("active");
});

$(".search_section .close_button , .search_section .overlay__").on("click",function(e){
    e.preventDefault();
    $(".search_section").removeClass("active");
})
}
